<?php

namespace App\DataFixtures;


use App\Entity\Product as EntityProduct;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class Product extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $c1 = new EntityProduct();
        $c1->setNom("Bien 1");
        $c1->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c1->setPrice(290090);
        $c1->setImage("img1.jpg");
        
        $manager->persist($c1);

        $c2 = new EntityProduct();
        $c2->setNom("Bien 2");
        $c2->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c2->setImage("img2.jpg");
        $c2->setPrice(823000);
        // $product = new Product();
        $manager->persist($c2);

        $c3 = new EntityProduct();
        $c3->setNom("Bien 3");
        $c3->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c3->setImage("img3.jpg");
        $c3->setPrice(230008);
        // $product = new Product();
        $manager->persist($c3);

        $c4 = new EntityProduct();
        $c4->setNom("Bien 4");
        $c4->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c4->setImage("img4.jpg");
        $c4->setPrice(176000);
        // $product = new Product();
        $manager->persist($c4);

        $c5 = new EntityProduct();
        $c5->setNom("Bien 5");
        $c5->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c5->setImage("img5.jpg");
        $c5->setPrice(125600);
        // $product = new Product();
        $manager->persist($c5);


        $c6 = new EntityProduct();
        $c6->setNom("Bien 6");
        $c6->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c6->setImage("img6.jpg");
        $c6->setPrice(823000);
        // $product = new Product();
        $manager->persist($c6);


        $c7 = new EntityProduct();
        $c7->setNom("Bien 7");
        $c7->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c7->setImage("img7.jpg");
        $c7->setPrice(723000);
        // $product = new Product();
        $manager->persist($c7);

        $c8 = new EntityProduct();
        $c8->setNom("Bien 8");
        $c8->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c8->setImage("img8.jpg");
        $c8->setPrice(623000);
        // $product = new Product();
        $manager->persist($c8);

        $c9 = new EntityProduct();
        $c9->setNom("Bien 9");
        $c9->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c9->setImage("img9.jpg");
        $c9->setPrice(423000);
        // $product = new Product();
        $manager->persist($c9);

        $c10 = new EntityProduct();
        $c10->setNom("Bien 10");
        $c10->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c10->setImage("img10.jpg");
        $c10->setPrice(23000);
        // $product = new Product();
        $manager->persist($c10);

        $c11 = new EntityProduct();
        $c11->setNom("Bien 11");
        $c11->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c11->setImage("img11.jpg");
        $c11->setPrice(9087661);
        // $product = new Product();
        $manager->persist($c11);

        $c12 = new EntityProduct();
        $c12->setNom("Bien 12");
        $c12->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c12->setImage("img12.jpg");
        $c12->setPrice(98776134);
        // $product = new Product();
        $manager->persist($c12);

        $c13 = new EntityProduct();
        $c13->setNom("Bien 13");
        $c13->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c13->setImage("img13.jpg");
        $c13->setPrice(9087689);
        // $product = new Product();
        $manager->persist($c13);


        $c14 = new EntityProduct();
        $c14->setNom("Bien 14");
        $c14->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c14->setImage("img14.jpg");
        $c14->setPrice(97000);
        // $product = new Product();
        $manager->persist($c14);


        $c15 = new EntityProduct();
        $c15->setNom("Bien 15");
        $c15->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c15->setImage("img15.jpg");
        $c15->setPrice(896454);
        // $product = new Product();
        $manager->persist($c15);


        $c16 = new EntityProduct();
        $c16->setNom("Bien 16");
        $c16->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c16->setImage("img16.jpg");
        $c16->setPrice(3413000);
        // $product = new Product();
        $manager->persist($c16);


        $c17 = new EntityProduct();
        $c17->setNom("Bien 17");
        $c17->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c17->setImage("img17.jpg");
        $c17->setPrice(90000);
        // $product = new Product();
        $manager->persist($c17);


        $c18 = new EntityProduct();
        $c18->setNom("Bien 18");
        $c18->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c18->setImage("img18.jpg");
        $c18->setPrice(9087000);
        // $product = new Product();
        $manager->persist($c18);


        $c19 = new EntityProduct();
        $c19->setNom("Bien 19");
        $c19->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c19->setImage("img19.jpg");
        $c19->setPrice(8923000);
        // $product = new Product();
        $manager->persist($c19);


        $c20 = new EntityProduct();
        $c20->setNom("Bien 20");
        $c20->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c20->setImage("img20.jpg");
        $c20->setPrice(78500987);
        // $product = new Product();
        $manager->persist($c20);


        $c21 = new EntityProduct();
        $c21->setNom("Bien 21");
        $c21->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c21->setImage("img21.jpg");
        $c21->setPrice(523000);
        // $product = new Product();
        $manager->persist($c21);

        $c22 = new EntityProduct();
        $c22->setNom("Bien 22");
        $c22->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c22->setImage("img22.jpg");
        $c22->setPrice(423000);
        // $product = new Product();
        $manager->persist($c22);



        $manager->flush();
    }
}
