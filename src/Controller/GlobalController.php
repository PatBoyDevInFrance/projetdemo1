<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GlobalController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function index(ProductRepository $productRepository): Response
    {   
        $immos = $productRepository->findAll();
        return $this->render('global/accueil.html.twig', [
            'immos' => $immos
        ]);
    }
}
