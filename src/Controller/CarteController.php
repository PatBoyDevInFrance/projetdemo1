<?php

namespace App\Controller;

use App\Entity\RechercheParPrix;
use App\Form\RechercheParPrixType;
use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CarteController extends AbstractController
{
    /**
     * @Route("/carte", name="carte")
     */
    public function index(ProductRepository $repo, PaginatorInterface $paginatorInterface, Request $request): Response
    {       
        $rechercheParPrix = new RechercheParPrix();
        $form = $this->createForm(RechercheParPrixType::class, $rechercheParPrix);
        $form->handleRequest($request);

        $carte = $paginatorInterface->paginate(
            $repo->findAllWithPaginator($rechercheParPrix), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            6 /*limit per page*/
        );
        return $this->render('carte/carte.html.twig', [
            'immos' => $carte,
            'form' => $form->createView(),
            'admin' => false
        ]);
    }
}
