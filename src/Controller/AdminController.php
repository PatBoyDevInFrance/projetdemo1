<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\RechercheParPrix;
use App\Form\AdminType;
use App\Form\RechercheParPrixType;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(ProductRepository $repo, PaginatorInterface $paginatorInterface, Request $request): Response
    {       
        $rechercheParPrix = new RechercheParPrix();
        $form = $this->createForm(RechercheParPrixType::class, $rechercheParPrix);
        $form->handleRequest($request);

        $carte = $paginatorInterface->paginate(
            $repo->findAllWithPaginator($rechercheParPrix), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            6 /*limit per page*/
        );
        return $this->render('carte/carte.html.twig',[
            'immos' => $carte,
            'form' => $form->createView(),
            'admin' => true

        ]);
    }

    /**
     * @Route("/admin/{id}", name="modifProduct")
     */
    public function modifier(Product $product,EntityManagerInterface $em ,Request $request): Response
    { 
        $form = $this->createForm(AdminType::class, $product);
        $form->handleRequest($request);
       
        
        if($form->isSubmitted() && $form->isValid()){
            $em->persist($product);
            $em->flush();
            return $this->redirectToRoute('admin');
        }

    return $this->render('admin/modifier.html.twig',[
        'immos' => $product,
        'form' => $form->createView(),
        
    ]);

    }


}
